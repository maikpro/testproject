package de.polizei.schichtplaner.model;

import java.util.Objects;

public class Example extends BaseEntity {
    private String exampleText;

    public Example(){}

    public Example(String exampleText) {
        this.exampleText = exampleText;
    }

    private Example(ExampleBuilder exampleBuilder){
        setExampleText(exampleBuilder.exampleText);
    }

    public void setExampleText(String exampleText){
        this.exampleText = exampleText;
    }

    public String getExampleText() {
        return this.exampleText;
    }

    @Override
    public String toString() {
        return "Example{" +
                "exampleText='" + exampleText + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Example example = (Example) o;

        return Objects.equals(exampleText, example.exampleText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.exampleText);
    }

    public static class ExampleBuilder {
        private String exampleText;

        public ExampleBuilder exampleText(String exampleText){
            this.exampleText=exampleText;
            return this;
        }

        public Example build(){
            return new Example(this);
        }
    }
}
