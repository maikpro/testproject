package de.polizei.schichtplaner.services.example;

import de.polizei.schichtplaner.model.Example;
import org.springframework.stereotype.Service;

@Service
public class ExampleService {
    public Example getAllExamples(){
        Example example = new Example.ExampleBuilder()
                .exampleText("Hello World")
                .build();

        System.out.println(example.toString());

        return example;
    }
}
