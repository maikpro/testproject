package de.polizei.schichtplaner.api;

public abstract class SchichtplanerRestController {
    public static final String ADMIN_PATH = "/admin";
    public static final String LEADER_PATH = "/leader";
}
