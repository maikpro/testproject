package de.polizei.schichtplaner.api.example;

import de.polizei.schichtplaner.model.Example;
import org.springframework.context.annotation.Configuration;

//Idee: Stattdessen MapStruct nutzen?
//Quelle: https://mapstruct.org/documentation/stable/api/org/mapstruct/InheritInverseConfiguration.html
@Configuration
public class ExampleMapperImpl implements ExampleMapper{
    @Override
    public ExampleDto map(Example example) {
        if( example == null ){
            return null;
        }

        ExampleDto exampleDto = new ExampleDto();
        exampleDto.setExampleText( example.getExampleText() );
        return exampleDto;
    }

    @Override
    public Example map(ExampleDto exampleDto) {
        if(exampleDto == null){
            return null;
        }

        Example example = new Example();
        example.setExampleText(exampleDto.getExampleText());
        return example;
    }
}
