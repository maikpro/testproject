package de.polizei.schichtplaner.api.example;


import de.polizei.schichtplaner.model.Example;

//@Mapper(config = MapperConfiguration.class)
public interface ExampleMapper {
    ExampleDto map(Example example);

    //@InheritedInverseConfiguration => MapStruct Modul?
    Example map(ExampleDto exampleDto);
}
