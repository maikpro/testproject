package de.polizei.schichtplaner.api.example;

import de.polizei.schichtplaner.api.SchichtplanerRestController;
import de.polizei.schichtplaner.model.Example;
import de.polizei.schichtplaner.services.example.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ExampleController.TEST_CONTROLLER_PATH)
public class ExampleController extends SchichtplanerRestController {
    public static final String TEST_CONTROLLER_PATH = SchichtplanerRestController.ADMIN_PATH + "/api";

    private final ExampleService exampleService;
    private final ExampleMapper exampleMapper;

    @Autowired
    public ExampleController(ExampleService exampleService, ExampleMapper exampleMapper) {
        this.exampleService = exampleService;
        this.exampleMapper = new ExampleMapperImpl();
    }

    //http://localhost:8080/admin/api/get
    @GetMapping("/get")
    public ResponseEntity<ExampleDto> getExample(){
        Example example = this.exampleService.getAllExamples();
        ExampleDto exampleDto = this.exampleMapper.map(example);
        return ResponseEntity.ok(exampleDto);

        /*String test = "Hello World";
        return ResponseEntity.ok(test);*/
    }

    //Postman nutzen => http://localhost:8080/admin/api/post/helloWorld
    @PostMapping("/post/{message}")
    @ResponseBody
    public String postExample(@PathVariable("message") String message){
        return message;
    }
}
