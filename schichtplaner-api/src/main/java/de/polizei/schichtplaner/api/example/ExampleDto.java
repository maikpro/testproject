package de.polizei.schichtplaner.api.example;

public class ExampleDto {
    private String exampleText;

    public void setExampleText(String exampleText) {
        this.exampleText = exampleText;
    }

    public String getExampleText() {
        return this.exampleText;
    }

    @Override
    public String toString() {
        return "ExampleDto{" +
                "exampleText='" + exampleText + '\'' +
                '}';
    }
}
